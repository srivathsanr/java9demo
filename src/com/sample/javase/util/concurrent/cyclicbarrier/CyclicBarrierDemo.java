package com.sample.javase.util.concurrent.cyclicbarrier;

import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {

	public static void main(String args[]) {
		CyclicBarrier barrier = new CyclicBarrier(3, new BarrierAction());
		new Thread(new MyThread(barrier,"ThreadA")).start();
		new Thread(new MyThread(barrier,"ThreadB")).start();
		new Thread(new MyThread(barrier,"ThreadC")).start();
		new Thread(new MyThread(barrier,"ThreadX")).start();
		new Thread(new MyThread(barrier,"ThreadY")).start();
		new Thread(new MyThread(barrier,"ThreadZ")).start();
	}
	
}
