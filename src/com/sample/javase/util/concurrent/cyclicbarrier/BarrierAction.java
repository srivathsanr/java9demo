package com.sample.javase.util.concurrent.cyclicbarrier;

public class BarrierAction implements Runnable {

	@Override
	public void run() {
		System.out.println("Barrier reached; This action will run now !");
	}

}
