package com.sample.javase.util.concurrent.semaphore.producerconsumer;

public class Producer implements Runnable {
	ProducerConsumerQueue queue;
	
	Producer(ProducerConsumerQueue queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		for(int i=0;i<100;i++) {
			try {
				queue.put(i);
				Thread.sleep(500);
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}
	}
	
}
