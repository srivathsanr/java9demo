package com.sample.javase.util.concurrent.semaphore.producerconsumer;

import java.util.concurrent.Semaphore;

public class ProducerConsumerQueue {

	int counter; 
	
	Semaphore producerSemaphore = new Semaphore(1);
	Semaphore consumerSemaphore = new Semaphore(0);
	
	void put(int n) throws Exception {
		producerSemaphore.acquire();
		this.counter = n;
		System.out.println("Put " + counter);
		consumerSemaphore.release();
	}
	
	void get() throws Exception {
		consumerSemaphore.acquire();
		System.out.println("Got " + counter);
		producerSemaphore.release();
	}
	
}
