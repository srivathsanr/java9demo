package com.sample.javase.util.concurrent.semaphore.producerconsumer;

public class ProducerConsumerDemo {

	public static void main(String args[]) {
		
		ProducerConsumerQueue queue = new ProducerConsumerQueue();
		
		new Thread(new Consumer(queue)).start();
		new Thread(new Producer(queue)).start();
	}
	
}
