package com.sample.javase.util.concurrent.semaphore.producerconsumer;

public class Consumer implements Runnable {

	ProducerConsumerQueue queue;
	
	public Consumer(ProducerConsumerQueue queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		for(int i=0;i<100;i++) {
			try {
				queue.get();
				Thread.sleep(500);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
