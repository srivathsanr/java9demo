package com.sample.javase.util.concurrent.semaphore;

import java.util.concurrent.Semaphore;

public class DecrementingThread implements Runnable {

	private Semaphore sem;
	private String name;
	
	DecrementingThread(Semaphore sem, String name) {
		this.sem = sem;
		this.name = name;
	}
	
	@Override
	public void run() {
		try {
			System.out.println(name + " waiting for lock");
			sem.acquire(); // Comment this line for Run 2. This will show how this code works without semaphores. The execution order is different everytime the program runs.
			System.out.println(name + " acquired lock");
			for(int i=0; i< 5; i++) {
				//Thread.sleep(1000L); // Uncomment this line if you want to see the execution at a slower pace
				SharedResource.count--;
				System.out.println(name + " changed count to " + SharedResource.count);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(name + " about to release lock");
		sem.release(); // Comment this line for Run 2. This will show how this code works without semaphores. The execution order is different everytime the program runs.
		System.out.println(name + " released lock");
	}

}
