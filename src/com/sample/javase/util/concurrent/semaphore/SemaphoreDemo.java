package com.sample.javase.util.concurrent.semaphore;

import java.util.concurrent.Semaphore;

public class SemaphoreDemo {

	public static void main(String args[]) throws InterruptedException {
		Semaphore sem = new Semaphore(1); // Only allow one thread at a time
		new Thread(new IncrementingThread(sem, "A")).start();
		new Thread(new DecrementingThread(sem, "B")).start();
	}
	
}
