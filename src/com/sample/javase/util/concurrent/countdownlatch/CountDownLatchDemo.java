package com.sample.javase.util.concurrent.countdownlatch;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchDemo {

	public static void main(String args[]) { 
		CountDownLatch latch = new CountDownLatch(1000);
		new Thread(new MyThread(latch)).start();
		try {
			System.out.println("Before await");
			latch.await();
			System.out.println("After await");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Done");
	}
	
}
