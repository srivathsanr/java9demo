package com.sample.javase.util.concurrent.countdownlatch;

import java.util.concurrent.CountDownLatch;

public class MyThread implements Runnable {

	CountDownLatch latch;
	
	public MyThread(CountDownLatch latch) { 
		this.latch = latch;
	}
	
	@Override
	public void run() {
		
		for(int i=0;i<1000;i++) {
			System.out.println(i);
			latch.countDown();
		}
		
	}
	

}
