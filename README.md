# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository has samples for all JDK features including collections, concurrency, IO, NIO, Streams, lambdas, etc. 
I collected these samples when I was preparing for my interviews. 
This is a work in progress and I will keep adding more examples as I find time.